# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.core import validators
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
# from django.core.signing import Signer, BadSignature, SignatureExpired
from django.core import signing


class SoftUserManager(BaseUserManager):
    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class SoftUser(AbstractBaseUser, PermissionsMixin):
    NO_SEX = 0
    SEX_MALE = 1
    SEX_FEMALE = 2
    SEX_CHOICES = (
        (NO_SEX, ''),
        (SEX_MALE, _('men')),
        (SEX_FEMALE, _('women')),
    )
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), unique=True)
    birth_date = models.DateField(_('birth date'), blank=True, null=True)
    sex = models.SmallIntegerField(_('sex'), choices=SEX_CHOICES, null=True, default=0)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                   'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                    'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    confirmed_registration = models.BooleanField(_('confirmed registration'), default=False)

    objects = SoftUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def __unicode__(self):
        return "%s %s" % (self.first_name, self.last_name)

    def create_confirm_link(self, salt):
        return reverse('confirm_registration', kwargs={'key': self.make_salt_key(salt)})

    def create_recovery_link(self, salt):
        return reverse('password_recovery', kwargs={'key': self.make_timestamp_salt_key(salt)})

    def make_salt_key(self, salt):
        return signing.Signer(salt).sign(self.id)

    def make_timestamp_salt_key(self, salt):
        return signing.dumps({"id": self.id, "last_login": str(self.last_login)}, salt=salt)

    @staticmethod
    def check_registration_key(salt, string):
        try:
            return signing.Signer(salt).unsign(string)
        except signing.BadSignature:
            return False

    @staticmethod
    def check_recovery_key(string, salt, max_age=0):
        try:
            return signing.loads(string, salt=salt, max_age=max_age)
        except signing.BadSignature, signing.SignatureExpired:
            return False