# -*- coding: utf-8 -*-
from django.contrib import messages
from django.core.signing import SignatureExpired, BadSignature
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect, render
from .forms import RegistrationForm, LoginForm, ConfirmRecoveryForm, RecoveryForm
from django.views.generic.edit import FormView
from django.contrib.auth import get_user_model, authenticate, login
from profiles.models import Profile
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail, BadHeaderError
from django.conf import settings
from django.http import Http404


class RegistrationView(FormView):
    template_name = 'registration.html'
    form_class = RegistrationForm
    success_url = '/registration/'

    def form_valid(self, form):
        user = get_user_model()._default_manager.create_user(email=form.cleaned_data['email'],
                                                             password=form.cleaned_data['password1'],
                                                             first_name=form.cleaned_data['first_name'],)
        Profile.objects.create(user=user)
        messages.success(
            self.request,
            _(u'На почтовый ящик %s отправленна ссылка для подтверждения регистрации.')
            % form.cleaned_data['email']
        )
        link = "%s://%s%s" % (self.request.scheme, self.request.get_host(),
                              user.create_confirm_link(salt='registration­confirm'), )
        try:
            send_mail(
                _(u'Подтверждение регистрации %s') % settings.MICROSOCIAL_NAME,
                _(u'Перейдите по ссылке %s для подтверждения регистрации.') % link,
                settings.MICROSOCIAL_NAME,
                [form.cleaned_data['email']],
            )
        except BadHeaderError as e:
            print e

        return super(RegistrationView, self).form_valid(form)


class ConfirmRecoveryView(FormView):
    template_name = 'confirm_recovery.html'
    form_class = ConfirmRecoveryForm
    success_url = '/password-recovery/'

    def form_valid(self, form):
        user = get_user_model()._default_manager.get(email=form.cleaned_data['email'])
        messages.success(
            self.request,
            _(u'На почтовый ящик %s отправленна ссылка для восстановления пароля.')
            % form.cleaned_data['email']
        )
        link = '%s://%s%s' % (self.request.scheme, self.request.get_host(),
                              user.create_recovery_link(salt='password-recovery-confirm'))
        try:
            send_mail(
                _(u'Восстановление пароля %s') % settings.MICROSOCIAL_NAME,
                _(u' %s для восстановления пароля.') % link,
                settings.MICROSOCIAL_NAME,
                [form.cleaned_data['email']],
            )
        except BadHeaderError as e:
            print e

        return super(ConfirmRecoveryView, self).form_valid(form)


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm
    success_url = '/'

    def post(self, request):
        form = LoginForm(data=request.POST)
        username = request.POST['email']
        password = request.POST['password']
        user = authenticate(email=username, password=password)
        if user is not None:
            if user.is_active and user.confirmed_registration:
                login(request, user)
                return redirect(self.success_url)
            else:
                messages.error(request, _(u'Аккаунт заблокирован.'))
                return render(request, 'login.html', {'form': form})
        else:
            messages.error(request, _(u'Неверный логин или пароль.'))
            return render(request, 'login.html', {'form': form})


def confirm_registration(request, key):
    user_model = get_user_model()
    user_id = user_model.check_registration_key(salt='registration­confirm', string=key)
    if not user_id:
        raise Http404
    user = get_object_or_404(user_model, pk=user_id, confirmed_registration=False)
    messages.success(request, _(u'Регистрация успешно завершена. Для входа введите email и пароль.'))
    user.confirmed_registration = True
    user.save(update_fields=['confirmed_registration'])
    return redirect(reverse('login'))


class RecoveryView(FormView):
    template_name = 'password_recovery.html'
    form_class = RecoveryForm
    success_url = '/'
    _hours = 48

    def get(self, request, *args, **kwargs):
        user_model = get_user_model()
        user_data = user_model.check_recovery_key(string=self.kwargs['key'],
                                                  salt='password-recovery-confirm', max_age=3600 * self._hours)
        if not user_data:
            raise Http404
        try:
            user = user_model._default_manager.get(pk=user_data['id'])
        except user_model.DoesNotExist:
            messages.error(self.request, _(u'Что-то пошло не так. Обратись в службу поддержки.'))
            return redirect(reverse('login'))
        if str(user.last_login) != user_data['last_login']:
            raise Http404
        if not user.confirmed_registration:
            user.confirmed_registration = True
            user.save(update_fields=['confirmed_registration'])
        return super(RecoveryView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        user_model = get_user_model()
        user_data = user_model.check_recovery_key(string=self.kwargs['key'],
                                                  salt='password-recovery-confirm', max_age=3600 * self._hours)
        user = user_model._default_manager.get(pk=user_data['id'])
        user.set_password(form.cleaned_data['password1'])
        user.save()
        user = authenticate(email=user.email, password=form.cleaned_data['password1'])
        login(self.request, user)
        messages.success(self.request, _(u'Пароль успешно изменен.'))
        return redirect(reverse('root'))


