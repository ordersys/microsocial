from django.conf.urls import patterns, url
from .views import RegistrationView


urlpatterns = patterns(
    '',
    url(
        r'^$',
        RegistrationView.as_view(),
        name='registration',
    ),
    url(
        r'(?P<key>.+?)/$',
        'soft_user.views.confirm_registration',
        name='confirm_registration',
    ),
)