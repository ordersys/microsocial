# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from django.core.validators import EmailValidator
from soft_user.models import SoftUser


class RegistrationForm(forms.Form):

    error_messages = {
        'email_exist': _(u'Email уже используется.'),
        'password_mismatch': _("The two password fields didn't match."),
    }
    first_name = forms.RegexField(label=_(u'имя'), max_length=30,
        regex=r'^[\w.@+-]+$',
        widget=forms.TextInput({"placeholder": _(u'имя'), "class": "form-control"}),
        error_messages={
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    email = forms.EmailField(label='email', validators=[EmailValidator],
                             widget=forms.EmailInput({"placeholder": "email", "class": "form-control"}),
                             error_messages={'invalid': _("Enter correct email address.")})
    password1 = forms.CharField(label=_(u'пароль'),
        widget=forms.PasswordInput({"placeholder": _(u'пароль'), "class": "form-control"}))
    password2 = forms.CharField(label=_(u'подтверждение пароля'),
        widget=forms.PasswordInput({"placeholder": _(u'подтверждение пароля'), "class": "form-control"}))

    def clean_email(self):
        email = self.cleaned_data['email'].strip()
        if SoftUser.objects.filter(email=email).exists():
            raise forms.ValidationError(
                self.error_messages['email_exist'],
                code='email_exist',
            )
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class LoginForm(forms.Form):

    email = forms.EmailField(label='email', validators=[EmailValidator],
                             widget=forms.EmailInput({"placeholder": "email", "class": "form-control"}),)
    password = forms.CharField(label=_(u'пароль'),
        widget=forms.PasswordInput({"placeholder": _(u'пароль'), "class": "form-control"}))


class ConfirmRecoveryForm(forms.Form):

    error_messages = {
        'no_email': _("A user with that email not exists."),
    }
    email = forms.EmailField(label='email', validators=[EmailValidator],
                             widget=forms.EmailInput({"placeholder": "email", "class": "form-control"}),
                             error_messages={'invalid': _("Enter correct email address.")})

    def clean_email(self):
        email = self.cleaned_data['email'].strip()
        if not SoftUser.objects.filter(email=email).exists():
            raise forms.ValidationError(
                self.error_messages['no_email'],
                code='no_email',
            )
        return email


class RecoveryForm(forms.Form):

    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput({"placeholder": _("Password"), "class": "form-control"}))
    password2 = forms.CharField(label=_("Password confirmation"),
        widget=forms.PasswordInput({"placeholder": _("Password confirmation"), "class": "form-control"}))

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2


class ChangePassForm(forms.Form):
    error_messages = {
        'password_wrong': _(u'Неверный пароль'),
        'password_mismatch': _("The two password fields didn't match."),
    }
    password = forms.CharField(label=_(u'текущий пароль'),
                               widget=forms.PasswordInput({"placeholder": _(u'текущий пароль'),
                                                           "class": "form-control"}))
    password1 = forms.CharField(label=_(u'новый пароль'),
                                widget=forms.PasswordInput({"placeholder": _(u'новый пароль'),
                                                            "class": "form-control"}))
    password2 = forms.CharField(label=_(u'новый пароль повторно'),
                                widget=forms.PasswordInput({"placeholder": _(u'новый пароль повторно'),
                                                            "class": "form-control"}))

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(ChangePassForm, self).__init__(*args, **kwargs)

    def clean_password(self):
        password = self.cleaned_data.get("password")
        if self.user.check_password(password):
            return password
        raise forms.ValidationError(
            self.error_messages['password_wrong'],
            code='password_wrong',
        )

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['password1'])
        if commit:
            self.user.save(update_fields=['password'])
        return self.user


class ChangeEmailForm(forms.Form):
    error_messages = {
        'password_wrong': _(u'Неверный пароль.'),
        'email_exist': _(u'Email уже используется.'),
    }
    new_email = forms.EmailField(label=_(u'email'),
                                 widget=forms.EmailInput({"placeholder": _(u'новый email'),
                                                      "class": "form-control"}))
    password = forms.CharField(label=_("current password"),
                               widget=forms.PasswordInput({"placeholder": _(u'текущий пароль'),
                                                           "class": "form-control"}))

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(ChangeEmailForm, self).__init__(*args, **kwargs)

    def clean_new_email(self):
        new_email = self.cleaned_data['new_email'].strip()
        if SoftUser.objects.filter(email=new_email).exists():
            raise forms.ValidationError(
                self.error_messages['email_exist'],
                code='email_exist',
            )
        return new_email

    def clean_password(self):
        password = self.cleaned_data.get("password")
        if self.user.check_password(password):
            return password
        raise forms.ValidationError(
            self.error_messages['password_wrong'],
            code='password_wrong',
        )

    def save(self, commit=True):
        self.user.email = self.cleaned_data['new_email']
        if commit:
            self.user.save(update_fields=['email'])
        return self.user