from django.contrib import admin
from .models import Profile, FriendInvitation


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'city', 'company',)

admin.site.register(Profile, ProfileAdmin)
admin.site.register(FriendInvitation)

