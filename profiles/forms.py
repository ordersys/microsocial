# -*- coding:utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from soft_user.models import SoftUser


class SettingsForm(forms.Form):
    first_name = forms.CharField(label=_(u'имя'), max_length=30,
                                 widget=forms.TextInput({"class": "form-control"}))
    last_name = forms.CharField(label=_(u'фамилия'), max_length=30, required=False,
                                widget=forms.TextInput({"class": "form-control"}))
    birth_date = forms.DateField(label=_(u'дата рождения'), required=False,
                                 widget=forms.DateInput({"class": "form-control"}))
    sex = forms.ChoiceField(label=_(u'пол'), choices=SoftUser.SEX_CHOICES, required=False,
                            widget=forms.Select({"class": "form-control"}))
    city = forms.CharField(label=_(u'город'), max_length=50, required=False,
                           widget=forms.TextInput({"class": "form-control"}))
    company = forms.CharField(label=_(u'место работы'), max_length=100, required=False,
                              widget=forms.TextInput({"class": "form-control"}))
    about = forms.CharField(label=_(u'о себе'), required=False,
                            widget=forms.Textarea({"class": "form-control", "rows": "3"}))
    interests = forms.CharField(label=_(u'интересы'), required=False,
                                widget=forms.Textarea({"class": "form-control", "rows": "3"}))
    avatar = forms.ImageField(label=_(u'аватар'), required=False, widget=forms.ClearableFileInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SettingsForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        self.user.first_name = self.cleaned_data['first_name']
        self.user.last_name = self.cleaned_data['last_name']
        self.user.birth_date = self.cleaned_data['birth_date']
        self.user.sex = self.cleaned_data['sex']
        self.user.profile.city = self.cleaned_data['city']
        self.user.profile.company = self.cleaned_data['company']
        self.user.profile.about = self.cleaned_data['about']
        self.user.profile.interests = self.cleaned_data['interests']
        self.user.profile.avatar = self.cleaned_data['avatar'] or None
        if commit:
            self.user.save()
            self.user.profile.save()
        return self.user

