from django.conf.urls import patterns, include, url
from .views import ProfileDetail


urlpatterns = patterns(
    '',
    url(
        r'^(?P<pk>\d+)/$',
        ProfileDetail.as_view(),
        name='profile'
    ),
)