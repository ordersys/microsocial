# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q
from django.db.transaction import atomic
from soft_user.models import SoftUser
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _
import os


def get_ids_from_profiles(*profiles):
    return [profiles.pk if isinstance(profile, Profile) else int(profile) for profile in profiles]


def make_avatar_fn(instance, filename):
    id_str = str(instance.pk)
    return 'avatars/{sub_dir}/{id}_{rnd}{ext}'.format(
        sub_dir=id_str.zfill(2)[-2:],
        id=id_str,
        rnd=get_random_string(8, 'abcdefghijklmnopqrstuvwxyz0123456789'),
        ext=os.path.splitext(filename)[1],
    )


class ProfileFriendShipManager(models.Manager):

    def are_friends(self, profile1, profile2):
        profile1_id, profile2_id = get_ids_from_profiles(profile1, profile2)
        return self.filter(pk=profile1_id, friends__pk=profile2_id).exists()

    @atomic
    def add(self, profile1, profile2):
        profile1_id, profile2_id = get_ids_from_profiles(profile1, profile2)
        if profile1_id == profile2_id:
            raise ValueError(_(u'Вы себе уже друг.'))
        if not self.are_friends(profile1, profile2):
            through_model = self.model.friends.through
            through_model.objects.bulk_create([
                through_model(from_profile_id=profile1_id, to_profile_id=profile2_id),
                through_model(from_profile_id=profile2_id, to_profile_id=profile1_id),
            ])
            FriendInvitation.objects.filter(
                Q(from_profile_id=profile1_id, to_profile_id=profile2_id) | Q(from_profile_id=profile2_id, to_profile_id=profile1_id)
            ).delete()
            return True

    def delete(self, profile1, profile2):
        profile1_id, profile2_id = get_ids_from_profiles(profile1, profile2)
        if self.are_friends(profile1, profile2):
            through_model = self.model.friends.through
            through_model.objects.filter(
                Q(from_profile_id=profile1_id, to_profile_id=profile2_id) | Q(from_profile_id=profile2_id, to_profile_id=profile1_id)
            ).delete()
            return True


class Profile(models.Model):
    user = models.OneToOneField(SoftUser, verbose_name=_(u'пользователь'), related_name='profile')
    city = models.CharField(verbose_name=_(u'город'), blank=True, max_length=50)
    company = models.CharField(verbose_name=_(u'компания'), blank=True, max_length=100)
    avatar = models.ImageField(_(u'изображение'), upload_to=make_avatar_fn, blank=True)
    friends = models.ManyToManyField('self', verbose_name=_(u'друзья'), symmetrical=True, blank=True)
    about = models.TextField(verbose_name=_(u'о себе'), blank=True)
    interests = models.TextField(verbose_name=_(u'интересы'), blank=True)

    friendship = ProfileFriendShipManager()

    class Meta:
        verbose_name = _(u'профиль')
        verbose_name_plural = _(u'профили')
        ordering = ('user',)

    def __unicode__(self):
        return "%s %s" % (self.user.first_name, self.user.last_name)


class FriendInvitationManager(models.Manager):
    def is_pending(self, from_profile, to_profile):
        from_profile_id, to_profile_id = get_ids_from_profiles(from_profile, to_profile)
        return self.filter(from_profile_id=from_profile_id, to_profile_id=to_profile_id).exists()

    def add(self, from_profile, to_profile):
        from_profile_id, to_profile_id = get_ids_from_profiles(from_profile, to_profile)
        if from_profile_id == to_profile_id:
            raise ValueError(_(u'Вы себе уже друг.'))
        if Profile.friendship.are_friends(from_profile_id, to_profile_id):
            raise ValueError(_(u'Вы уже друзья.'))
        if self.is_pending(from_profile_id, to_profile_id):
            raise ValueError(_(u'Заявка была создана и ожидает рассмотрения.'))
        if self.is_pending(to_profile_id, from_profile_id):
            Profile.friendship.add(from_profile_id, to_profile_id)
            return 2
        self.create(from_profile_id=from_profile_id, to_profile_id=to_profile_id)
        return 1

    def approve(self, from_profile, to_profile):
        from_profile_id, to_profile_id = get_ids_from_profiles(from_profile, to_profile)
        if not self.is_pending(from_profile_id, to_profile_id):
            raise ValueError(_(u'Заявка не существует.'))
        return Profile.friendship.add(from_profile_id, to_profile_id)

    def reject(self, from_profile, to_profile):
        from_profile_id, to_profile_id = get_ids_from_profiles(from_profile, to_profile)
        self.filter(from_profile_id=from_profile_id, to_profile_id=to_profile_id).delete()


class FriendInvitation(models.Model):
    from_profile = models.ForeignKey('profiles.Profile', related_name='outcoming_friend_invitations')
    to_profile = models.ForeignKey('profiles.Profile', related_name='incoming_friend_invitations')

    objects = FriendInvitationManager()

    class Meta:
        unique_together = ('from_profile', 'to_profile')
