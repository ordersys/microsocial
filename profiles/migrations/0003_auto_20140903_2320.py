# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_profile_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='about',
            field=models.TextField(default='', verbose_name='\u043e \u0441\u0435\u0431\u0435', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='interests',
            field=models.TextField(default='', verbose_name='\u0438\u043d\u0442\u0435\u0440\u0435\u0441\u044b', blank=True),
            preserve_default=False,
        ),
    ]
