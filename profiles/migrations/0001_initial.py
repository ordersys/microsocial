# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city', models.CharField(max_length=50, verbose_name='\u0433\u043e\u0440\u043e\u0434', blank=True)),
                ('company', models.CharField(max_length=100, verbose_name='\u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f', blank=True)),
                ('avatar', models.ImageField(upload_to=b'', verbose_name='\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('friends', models.ManyToManyField(related_name='friends_rel_+', verbose_name='\u0434\u0440\u0443\u0437\u044c\u044f', to='profiles.Profile', blank=True)),
            ],
            options={
                'ordering': ('user',),
                'verbose_name': '\u043f\u0440\u043e\u0444\u0438\u043b\u044c',
                'verbose_name_plural': '\u043f\u0440\u043e\u0444\u0438\u043b\u0438',
            },
            bases=(models.Model,),
        ),
    ]
