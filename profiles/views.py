# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth import login, BACKEND_SESSION_KEY
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from .models import Profile
from posts.models import Post
from .forms import SettingsForm
from soft_user.models import SoftUser
from soft_user.forms import ChangePassForm, ChangeEmailForm


class ProfileDetail(TemplateView):
    model = Profile
    template_name = "profiles/profile.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.user = get_object_or_404(SoftUser, pk=kwargs['pk'])
        return super(ProfileDetail, self).dispatch(request, *args, **kwargs)

    def get_wall_posts(self):
        paginator = Paginator(self.user.profile.wall_posts.select_related('created_by'), 20)
        page = self.request.GET.get('page')
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            posts = paginator.page(1)
        except EmptyPage:
            posts = paginator.page(paginator.num_pages)
        return posts

    def get_context_data(self, **kwargs):
        context = super(ProfileDetail, self).get_context_data(**kwargs)
        context['profile'] = self.user.profile
        context['posts'] = self.get_wall_posts()
        return context

    def post(self, request, *args, **kwargs):
        Post.objects.create(profile=Profile.objects.get(pk=kwargs['pk']), created_by=request.user.profile,
                            created_at=datetime.now(), text=request.POST['text'])
        self.object = Profile.objects.get(pk=kwargs['pk'])
        # context = self.get_context_data(**kwargs)
        # return self.render_to_response(context)
        return redirect(request.path)


class SettingsView(TemplateView):
    template_name = 'profiles/settings.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        action = request.POST.get('action')
        self.profile_form = SettingsForm(
            request.user,
            request.POST if action == 'profile' else None,
            request.FILES if action == 'profile' else None,
            initial={
                'first_name': request.user.first_name,
                'last_name': request.user.last_name,
                'birth_date': request.user.birth_date,
                'sex': request.user.sex,
                'city': request.user.profile.city,
                'company': request.user.profile.company,
                'about': request.user.profile.about,
                'interests': request.user.profile.interests,
                'avatar': request.user.profile.avatar,
        })
        self.form_pass_change = ChangePassForm(request.POST if action == 'pass' else None,
                                               user=request.user if action == 'pass' else None)
        self.form_email_change = ChangeEmailForm(request.POST if action == 'email' else None,
                                                 user=request.user)

        return super(SettingsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        context['profile_form'] = self.profile_form
        context['form_pass_change'] = self.form_pass_change
        context['form_email_change'] = self.form_email_change

        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(form=self.profile_form)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        if self.profile_form.is_valid():
            self.profile_form.save()
            messages.success(request, u'Профиль успешно сохранен.')
            return redirect(request.path)
        elif self.form_pass_change.is_valid():
            self.form_pass_change.save()
            request.user.backend = request.session[BACKEND_SESSION_KEY]
            login(request, request.user)
            messages.success(request, u'Пароль успешно изменен.')
            return redirect(request.path)
        elif self.form_email_change.is_valid():
            self.form_email_change.save()
            messages.success(request, u'Email успешно изменен.')
            return redirect(request.path)
        return self.get(request, *args, **kwargs)

