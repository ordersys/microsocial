# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Post(models.Model):
    profile = models.ForeignKey('profiles.Profile', verbose_name=_(u'на стене'), related_name='wall_posts')
    created_by = models.ForeignKey('profiles.Profile', verbose_name=_(u'опубликовал'), related_name='profile_posts')
    created_at = models.DateTimeField(verbose_name=_(u'время публикации'))
    text = models.TextField(_(u'текст'))

    class Meta:
        verbose_name = _(u'сообщение')
        verbose_name_plural = _(u'сообщения')
        ordering = ('-created_at',)

    def __unicode__(self):
        return u'(%s) %s' % (self.profile, self.text)
