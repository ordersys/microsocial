# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='profile',
            field=models.ForeignKey(related_name=b'wall_posts', verbose_name='\u043d\u0430 \u0441\u0442\u0435\u043d\u0435', to='profiles.Profile'),
        ),
    ]
