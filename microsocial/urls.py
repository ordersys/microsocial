# -*- coding:utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib import admin
from soft_user.views import LoginView, ConfirmRecoveryView, RecoveryView
from django.conf import settings
from profiles.views import SettingsView


urlpatterns = patterns(
    '',
    url(
        r'^$',
        'microsocial.views.root',
        name='root',
    ),
    url(
        r'^login/$',
        LoginView.as_view(),
        name='login',
    ),
    url(
        r'^logout/$',
        'django.contrib.auth.views.logout_then_login',
        name='logout',
    ),
    url(
        r'^profile/',
        include('profiles.urls'),
    ),
    url(
        r'^registration/',
        include('soft_user.urls'),
    ),
    url(
        r'^password-recovery/$',
        ConfirmRecoveryView.as_view(),
        name='confirm_recovery',
    ),
    url(
        r'^settings/$',
        SettingsView.as_view(),
        name='settings',
    ),
    url(
        r'^password-recovery/(?P<key>.+?)/$',
        RecoveryView.as_view(),
        name='password_recovery',
    ),
    url(
        r'^i18n/',
        include('django.conf.urls.i18n'),
    ),
    url(
        r'^admin/',
        include(admin.site.urls),
    ),

)

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += [
     url(
        r'',
        include('django.contrib.flatpages.urls'),
     ),
]
