# -*- coding:utf-8 -*-
from django import template


register = template.Library()


@register.inclusion_tag('posts.html', takes_context=True)
def show_posts(context, posts):
    return {'messages': context.get('messages'), 'posts': posts}

