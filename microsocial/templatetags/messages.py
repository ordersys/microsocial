from django import template


register = template.Library()


@register.inclusion_tag('messages.html', takes_context=True)
def messages(context, size=12, offset=0):
    return {'messages': context.get('messages'), 'size': size, 'offset': offset, }


@register.inclusion_tag('field_errors.html')
def field_errors(errors):
    return {'field_errors': errors}