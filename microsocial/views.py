from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required


@login_required(login_url='/login/')
def root(request):
    return redirect(reverse('profile', kwargs={'pk': request.user.profile.id}))

